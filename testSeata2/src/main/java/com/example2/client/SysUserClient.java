package com.example2.client;

import com.example2.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SysUserClient implements ISysUserClient {

    @Autowired
    private OrderServiceImpl orderService;

   public int addOrder(Double amount,String address){
       int count = orderService.addOrder(amount, address);
       return count;
   }

}
