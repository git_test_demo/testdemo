package com.example.controller;

import com.example.config.DataSourceHolder;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/addUser")
    public String addUser(String name, Integer age) {
        try {
            DataSourceHolder.setDataSource("dataSource");
            userService.addUser(name, age);
        } finally {
            DataSourceHolder.clearDataSource();
        }
        return "成功";
    }

    @PostMapping(value = "/addOrder")
    public String addOrder(Double amount, String address) {
        try {
            DataSourceHolder.setDataSource("dataSource2");
            userService.addOrder(amount, address);
        } finally {
            DataSourceHolder.clearDataSource();
        }
        return "成功";
    }
}
