package com.example.service;


import com.example.util.MasterException;
import com.example.util.ResultValue;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice(basePackages = "com.example")
public class ExceptionAdviceHandler {

    @ExceptionHandler({MasterException.class})
    @ResponseBody
    public ResultValue<?> handleException(MasterException e){
        ResultValue<?> resultValue = new ResultValue<>();
        resultValue.setSuccess(false);
        resultValue.setMessage(e.getMessage());
        return resultValue;
    }

   /* @ExceptionHandler({ArithmeticException.class})
    @ResponseBody
    public ResultValue<?> handleException(RuntimeException e){
        ResultValue<?> resultValue = new ResultValue<>();
        resultValue.setSuccess(false);
        resultValue.setMessage(e.getMessage());
        return resultValue;
    }*/
}
