package com.example.controller;

import com.example.service.ExceptionServiceImp;
import com.example.util.ResultValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ExceptionController {

    @Autowired
    private ExceptionServiceImp exceptionServiceImp;

    @GetMapping(value = "/test")
    public ResultValue test(String name) {
        Map map = exceptionServiceImp.test(name);
        return new ResultValue<>(map);
    }

    @GetMapping(value = "/test2")
    public ResultValue test2(int a,int b) {
        int count = exceptionServiceImp.test2(a, b);
        return new ResultValue<>(count);
    }
}
